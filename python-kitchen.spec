%global with_python3 1
%global python3_pkgversion 3

Name:           python-kitchen
Version:        1.2.6
Release:        9
Summary:        Small, useful pieces of code to make python coding easier

License:        LGPLv2+
URL:            https://pypi.python.org/pypi/kitchen/
Source0:        https://files.pythonhosted.org/packages/d9/ca/3365cb1160533be8c8b57dbfd6502f367d35e30935ee89a003c664740714/kitchen-1.2.6.tar.gz

BuildArch:      noarch

BuildRequires: python-sphinx
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-test
BuildRequires:  python%{python3_pkgversion}-chardet

Patch0: 0001-replace-obsolete-pngmath-with-imgmath.patch
Patch1: Adapt-to-intersphinx-mapping-format-due-to-Sphinx-up.patch

%description
kitchen includes functions to make gettext easier to use, handling unicode
text easier (conversion with bytes, outputting xml, and calculating how many
columns a string takes), and compatibility modules for writing code that uses
python-2.7 modules but needs to run on python-2.3.

%if 0%{?with_python3}
%package -n python%{python3_pkgversion}-kitchen
Summary:    Small, useful pieces of code to make python 3 coding easier
%{?python_provide:%python_provide python%{python3_pkgversion}-kitchen}

Requires:   python%{python3_pkgversion}
Requires:   python%{python3_pkgversion}-chardet

%description -n python%{python3_pkgversion}-kitchen
kitchen includes functions to make gettext easier to use, handling unicode
text easier (conversion with bytes, outputting xml, and calculating how many
columns a string takes).

This is the python3 version of the kitchen module.

%package    help
Summary:    API documentation for the Kitchen python3 module

%description help
kitchen includes functions to make gettext easier to use, handling unicode
text easier (conversion with bytes, outputting xml, and calculating how many
columns a string takes).

This package contains the API documenation for programming with the
python-3 version of the kitchen library.
%endif


%prep
%autosetup -p1 -n kitchen-%{version}

# Remove bundled egg info, if any.
rm -rf *.egg*

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
%endif

%build

%if 0%{?with_python3}
pushd %{py3dir}
%{py3_build}
popd
%endif

# Build docs
pushd %{py3dir}
sphinx-build-3 kitchen3/docs/ build/sphinx/html
cp -pr build/sphinx/html .
find . -name html
ls ./html
rm -rf html/.buildinfo
popd

%install

%if 0%{?with_python3}
pushd %{py3dir}
%{py3_install}
popd
%endif

# %check
# # In current mock, the PATH isn't being reset.  This causes failures in some
# # subprocess tests as a check tests /root/bin/PROGRAM and fails with Permission
# # Denied instead of File Not Found.  reseting the PATH works around this.
# PATH=/bin:/usr/bin
# PYTHONPATH=.:kitchen3/ nosetests-%{python3_version} kitchen3/tests/

%if 0%{?with_python3}
%files -n python%{python3_pkgversion}-kitchen
%doc README.rst NEWS.rst
%license COPYING COPYING.LESSER
%{python3_sitelib}/kitchen*

%files help
%doc kitchen3/docs/*
%license COPYING COPYING.LESSER
%endif

%changelog
* Tue Nov 26 2024 xu_ping <707078654@qq.com> - 1.2.6-9
- fix build error due to python-sphinx upgrade 

* Wed Oct 26 2022 zhangruifang <zhangruifang1@h-partners.com> - 1.2.6-8
- Rebuild for next release

* Tue Mar 15 2022 shixuantong <shixuantong@h-partners.com> - 1.2.6-7
- rename python3-kitchen-help to python-kitchen-help

* Mon Feb 21 2022 shixuantong <shixuantong@h-partners.com> - 1.2.6-6
- rename python-kitchen-doc to python-kitchen-help

* Sat Jan 08 2022 shixuantong <shixuantong@huawei.com> - 1.2.6-5
- remove python3-nose buildrequire

* Wed Dec 09 2020 tianwei <tianwei12@huawei.com> - 1.2.6-4
- delete sensitive word

* Thu Aug 06 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 1.2.6-3
- replace obsolete pngmath with imgmath

* Wed Aug 05 2020 Guoshuai Sun <sunguoshuai@huawei.com> - 1.2.6-2
- Del the python2 and fix build error

* Sat Feb 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.2.6-1
- Update to 1.2.6 and enable python3 test

* Sat Dec 21 2019 duyeyu <duyeyu@huawei.com> - 1.2.5-6
- initial package
